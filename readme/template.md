# <%= GLOBALS.APP_NAME %> (UI)

[![app](https://img.shields.io/static/v1?label=&message=<%= encodeURIComponent(GLOBALS.APP_NAME) %>&color=<%= GLOBALS.THEME.__THEME_DARK_COLOR__.substring(1) %>&logo=<%= logoImageUrl %>)](<%= parentRepositoryUrl %>)
[![app-ui](https://img.shields.io/static/v1?labelColor=<%= GLOBALS.THEME.__THEME_DARK_COLOR__.substring(1) %>&label=ui&message=v<%= GLOBALS.VERSION.CURRENT %>&color=<%= GLOBALS.THEME.__THEME_DARK_COLOR__.substring(1) %>&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8I////ev///+n/////////////////////////6v///+r/////////////////////////6f///3r///8I////Sf////D/////////////////////////zv///zX///81////zv/////////////////////////w////Sf///2f////8/////////////////////////9D///82////Nv///9D//////////////////////////P///2f///9n/////P/////////r////4P///+D////g////zv///87////g////4P///+D////r//////////z///9n////Z/////z////+////bv///xv///8f////H////yD///8g////H////x////8b////bv////7////8////Z////2f////8/////v///1n///8A////AP///wD///8A////AP///wD///8A////AP///1n////+/////P///2f///9n/////P////7///9a////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD///9a/////v////z///9n////Z/////z////+////Wv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////Wv////7////8////Z////2f////8/////v///1r///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AP///1r////+/////P///2f///9n/////P////7///9a////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD///9a/////v////z///9n////Z/////z////+////Wv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////Wv////7////8////Z////2f////8/////v///1r///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AP///1r////+/////P///2f///9n/////P////7///9Z////AP///wD///8A////AP///wD///8A////AP///wD///9Z/////v////z///9n////Z/////3////+////bv///xv///8f////H////x////8f////H////x////8b////bv////7////9////Z////0n////w/////////+v////g////4P///+D////g////4P///+D////g////4P///+v/////////8P///0n///8I////ev///+n/////////////////////////////////////////////////////////6f///3r///8IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB+AAAAfgAAAH4AAAB+AAAAfgAAAH4AAAAAAAAAAAAAAAAAAAAAAAAA==)](<%= projectUrl %>)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&labelColor=2496ED&message=Docker&color=2496ED)](https://docker.com/)
[![Vuejs](https://shields.io/static/v1?logo=vuedotjs&label=&labelColor=5B5B5B&message=Vue.js&color=5B5B5B)](https://vuejs.org/)
[![Webpack](https://shields.io/static/v1?logo=webpack&label=&labelColor=5B5B5B&message=Webpack&color=5B5B5B)](https://webpack.js.org/)
[![Cordova](https://shields.io/static/v1?logo=apachecordova&label=&labelColor=5B5B5B&message=Cordova&color=5B5B5B)](https://cordova.apache.org/)
[![Discord](https://img.shields.io/discord/<%= GLOBALS.SOCIAL.DISCORD_SERVER_ID %>?label=Discord&labelColor=5562EA&logo=discord&logoColor=white&color=2F3035)](<%= GLOBALS.SOCIAL.DISCORD_INVITE_LINK %>)
[![pipeline-status](<%= projectUrl %>/badges/main/pipeline.svg)](<%= projectUrl %>/commits/main)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

## Requirements

- Node v14.16.1
- NPM 6.14.12
- Cordova 10.0.0
- @vue/cli 4.5.15

## Get started

> **IMPORTANT**: Please refer to the parent repository: [app](<%= parentRepositoryUrl %>).

If you want to clone this repository only:

```bash
git clone <%= repositoryUrl %>
```

## Install

First, install `@vue/cli` and `cordova`

```bash
# vue-cli
npm install -g @vue/cli
# cordova
npm install -g cordova
```

> **WARNING**: cordova needs some requirements too (see documentation: [https://cordova.apache.org/docs/en/10.x/guide/cli/index.html](https://cordova.apache.org/docs/en/10.x/guide/cli/index.html))

Install project dependencies:

```bash
npm install
```

Then, prepare Cordova:

```bash
# Add Cordova platforms
npm run build # to create 'www/' folder
cordova platform add ios
cordova platform add android
cordova platform add electron
```

## Directory structure

Let's have a look to the directory structure:

```
ui/
├─ .vscode/
├─ bin/
├─ build/
├─ dist/
├─ lib/
├─ node_modules/
├─ public/
├─ readme/                   # For readme-generator
├─ src/
│  ├─ assets/                # All app assets
│  ├─ components/
│  ├─ config/                # See below for configuration
│  ├─ core/
│  ├─ i18n/
│  ├─ mixins/
│  ├─ models/
│  ├─ router/
│  ├─ scss/
│  │  ├─ _variables.scss_    # Variables file
│  │  └─ main.scss           # Main style entry
│  ├─ store/
│  │  ├─ modules/            # vuex store modules
│  │  ├─ plugins/            # vuex store plugins
│  │  └─ index.js
│  ├─ views/
│  └─ main.js
├─ tests/
├─ .eslintrc.js              # ESLint configuration
├─ .stylelintrc.json         # Stylelint configuration
├─ globals.config.json       # App globals definition
├─ jest.config.js            # jest (tests) configuration
├─ vue.config.js             # vue (+ webpack) configuration
└─ ...

```

## Development

```
# Start server
npm run dev
# or
npm run start
# or
npm run serve
```

## Production

```
# Build
npm run build
```

## Tests

```
# Unit
npm run test:unit
# End-to-end
npm run test:e2e
```

## Lints and fixes files

> **NOTE**: In development (via `npm run dev`), ESLint is automatically triggered at each file update by *webpack-dev-server*.

```
npm run lint
```

## Release

> **NOTE**: Local repository must be clean

- 1. Update CHANGELOG (see also [documentation](https://keepachangelog.com/en/1.0.0/))
- 2. New release (run command below)

```bash
# New release
npm run release
```

## Command list

> See also package.json file

```bash
npm run build # Build
npm run prebuild # Do nothing
npm run postbuild # Do nothing
npm run dev # npm run serve
npm run docs:generate # Generate docs (readme)
npm run info:version # Print current mystation-ui version
npm run version:new # Increments version (in globals.config.json & package.json) + git commit
npm run version:tag # Create new git tag
npm run release # Create new release
npm run serve # vue cli serve (development)
npm run start # vue cli serve (development)
npm run readme # Generate readme based on template
npm run test:unit # Start unit tests
npm run test:e2e # Start e2e tests
npm run lint # Lint code
```

## Globals

Let's have a look to the `globals.config.json` file:

```
ui/
├─ ...
├─ globals.config.json
└─ ...
```

```javascript
<%- globalsSource %>
```

Every global defined in this file can be accessed:
- .js files
  ```javascript
  // Example: theme global
  const color = __THEME_PRIMARY_COLOR__
  ```
- .vue files
  ```vue
  <!-- Example: theme global in <template> -->
  <p :style="color: `${GLOBALS.__THEME_PRIMARY_COLOR__};`">Text</p>
  ```
  ```js
  // Example: theme global in <script> tag of the component
  this.color = this.GLOBALS.__THEME_PRIMARY_COLOR__
  ```
- .scss files
  ```scss
  p {
    color: $__THEME_PRIMARY_COLOR__;
  }
  ```

## Configuration

```
ui
├─ ...
├─ src
│  ├─ config
│  │  ├─ config.js
└─ ...
```

### Options

```javascript
<%- configSource %>
```

## Internal flow

```mermaid
<%- internalFlowMmdSource %>
```

## Dependencies

<details>
<summary><b>Global</b> (click to expand)</summary>

<%= dependencies %>

</details>

<details>
<summary><b>Dev</b> (click to expand)</summary>

<%= devDependencies %>

</details>

## Docker

> **NOTE**: Remember that the docker images & containers are managed in the project top-level directory (/app): see [documentation](<%= parentRepositoryUrl %>).

### Build the image

```bash
docker build -t node-14.19.1 .
```

> **NOTE**: `node-14.19.1` will be the name of the image. You can modify it as you want.

### Run the container

> **IMPORTANT**
>
> Using docker container in <u>development</u> for this subproject ('UI') is not optimized in Windows environment!
>
> Webpack-dev-server hot reload is extremely slow when it used without **cached volume** due to the size of the `node_modules` folder.
>
> Let's have a look to the `docker-compose.yml` file in the [parent repository](<%= parentRepositoryUrl %>)! The *MODE* option for mounted volumes is only available for Linux or MacOS platform ([details](https://docs.docker.com/storage/bind-mounts/#configure-mount-consistency-for-macos)).
>
> Unfortunately, Windows does not support **cached volume**... Pray for Windows improvements in the future ¯\\_(ツ)_/¯
>
> There seems to be a workaround as explained in this [tutorial](https://www.youtube.com/watch?v=dQw4w9WgXcQ).
>
> More seriously, if you want to use Docker for this subproject without mounting the volume, comment the *volumes* section of the *ui* service in the `docker-compose.yml` file. Then, connect to the container via **SSH** once the container is up. You can use the Visual Studio Code extension [kelvin.vscode-sshfs](https://marketplace.visualstudio.com/items?itemName=Kelvin.vscode-sshfs) (as mentionned in the `.vscode/extensions.json` file) to connect to your container and to easily make your changes in your local machine.
>
> However, if you don't use Docker, you need to have NodeJS installed in your local machine. See [requirements](#requirements) section.

This command will run the PHP container named `ui` based on the `node-14.19.1` image previously created.

Note that the **container port** `8080` will be related to the **local port** `8080`.

```bash
docker run -d -p 8080:8080 --name ui node-14.19.1
```

### Using GitLab

Login to gitlab registry:

```bash
docker login registry.gitlab.com
```

Build and push image to gitlab registry:

```bash
# Build the image (using 'prod' stage, see Dockerfile)
docker build --target prod -t registry.gitlab.com/hperchec/boilerplates/scorpion/ui .
# Push the image to the container registry
docker push registry.gitlab.com/hperchec/boilerplates/scorpion/ui
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)