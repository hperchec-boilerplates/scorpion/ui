#!/usr/bin/env node

'use strict'

const yargonaut = require('yargonaut') // yargonaut first
const colors = require('colors') // eslint-disable-line

const newVersionCmd = require('../lib/new-version.js')
const createTagCmd = require('../lib/create-tag.js')
const commitCmd = require('../lib/commit.js')

console.log((`
--------------------------
App CLI
--------------------------
`).cyan)

console.log('Made with ' + (('❤').red) + '  by ' + 'Hervé Perchec <herve.perchec@gmail.com>'.yellow + '\n')

yargonaut.style('yellow')
  .helpStyle('cyan.underline')

var yargs = require('yargs')
  .scriptName("app-ui")
  .usage('app-ui [ release | version:<option> | --version | --help ]')
  .command({
    command: 'release',
    describe: 'Prepare new release: new version, commit and tag'
  })
  .command({
    command: 'version:new',
    describe: 'Create new version'
  })
  .command({
    command: 'version:tag',
    describe: 'Create new tag with current version'
  })
  .help()

const commands = yargs.getCommandInstance().getCommands()

var argv = yargs.argv

// Check if command is set
if (!argv._[0] || commands.indexOf(argv._[0]) === -1) {
  console.log(('Error : "' + argv._[0] + '" - non-existing or no command specified\n').red)
  yargs.showHelp()
  process.exit(1)
}

(async () => {
  switch (argv._[0]) {
    // release command
    case 'release':
      await newVersionCmd()
      const CURRENT_VERSION = require('../package.json').version
      commitCmd('Update v' + CURRENT_VERSION)
      createTagCmd()
      break
    // version:new command
    case 'version:new':
      newVersionCmd()
      break
    // version:tag command
    case 'version:tag':
      createTagCmd()
      break
  }
})()
