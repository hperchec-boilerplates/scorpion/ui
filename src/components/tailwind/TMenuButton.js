// Vue-tailwind component
import { TButton } from 'vue-tailwind/dist/components'

export default {
  component: TButton,
  props: {
    tagName: 'a',
    fixedClasses: 'inline-block px-2 py-2 transition duration-100 ease-in-out disabled:opacity-50 disabled:cursor-not-allowed',
    classes: 'text-white bg-hue border border-transparent shadow-sm rounded-xl w-full h-full',
    variants: {
      transparent: 'text-gray-400 bg-transparent border border-transparent shadow-sm hover:text-white'
    }
  }
}
