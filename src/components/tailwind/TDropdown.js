// Vue-tailwind component
import { TDropdown } from 'vue-tailwind/dist/components'

export default {
  component: TDropdown,
  props: {
    fixedClasses: {
      button: 'flex items-center text-white block px-4 py-2 transition duration-100 ease-in-out border border-transparent shadow-sm focus:border-blue-500 focus:ring-2 focus:ring-blue-500 focus:outline-none focus:ring-opacity-50 disabled:opacity-50 disabled:cursor-not-allowed',
      wrapper: 'inline-flex flex-col relative',
      dropdownWrapper: 'w-full relative z-40 -top-1',
      dropdown: 'origin-top-left absolute top-0 right-0 w-full shadow mt-1',
      enterClass: 'opacity-0 scale-95',
      // enterActiveClass: 'transition transform ease-in-out duration-100',
      enterActiveClass: '',
      enterToClass: 'opacity-100 scale-100',
      leaveClass: 'opacity-100 scale-100',
      // leaveActiveClass: 'transition transform ease-in-out duration-100',
      leaveActiveClass: '',
      leaveToClass: 'opacity-0 scale-95'
    },
    classes: {
      button: 'hover:bg-primary',
      dropdown: 'bg-transparent shadow-none'
    },
    variants: {
      //
    }
  }
}
