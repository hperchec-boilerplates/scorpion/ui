// Vue-tailwind component
import { TAlert } from 'vue-tailwind/dist/components'

export default {
  component: TAlert,
  props: {
    fixedClasses: {
      wrapper: 'relative flex items-center p-4 mb-8 px-6 w-full border-l-4 rounded shadow-sm',
      body: 'flex-grow',
      close: 'absolute relative flex items-center justify-center ml-4 flex-shrink-0 w-6 h-6 transition duration-100 ease-in-out rounded focus:ring-2 focus:ring-blue-500 focus:outline-none focus:ring-opacity-50',
      closeIcon: 'fill-current h-4 w-4'
    },
    classes: {
      wrapper: 'bg-gray-100 dark:bg-dark-2 border-primary',
      body: '',
      close: 'text-primary hover:bg-primary hover:text-light'
    },
    variants: {
      error: {
        wrapper: 'bg-gray-100 border-error',
        body: 'text-error',
        close: 'text-error hover:bg-error hover:text-light'
      }
    }
  }
}
