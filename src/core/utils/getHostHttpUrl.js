/**
 * getHostHttpUrl
 * @description Return server url
 * @example
 * getHostHttpUrl('http://example.com/blog/posts?id=1') // return 'http://example.com/'
 * getHostHttpUrl('example.com/blog/posts/') // fail
 * @param {string} str - The url string
 * @return {(string|boolean)}
 */
const getHostHttpUrl = function (str) {
  let url
  try {
    url = new URL(str)
  } catch (error) {
    return false
  }
  if (url.protocol === 'http:' || url.protocol === 'https:') {
    return url.protocol + '//' + url.host
  } else {
    return false
  }
}

module.exports = getHostHttpUrl
