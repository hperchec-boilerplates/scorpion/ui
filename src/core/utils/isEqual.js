// Lodash.minby
const lodashIsEqual = require('lodash.isequal')

/**
 * isEqual
 * @description Compare two values (lodash.isequal).
 * @example
 * isEqual(1, 2) // Return -> false
 * isEqual([ 'toto', 'tata' ], [ 'tata', 'toto' ]) // Return -> true
 * @param {*} value - The value to compare
 * @param {*} other - The other value to compare
 * @return {string}
 */
const isEqual = function (value, other) {
  return lodashIsEqual(value, other)
}

module.exports = isEqual
