// Lodash.debounce
const lodashDebounce = require('lodash.debounce')

/**
 * debounce
 * @description Debounce callback (lodash.debounce).
 * @example
 * debounce(myFunc, 150) // call 'myfunc()' after 150 ms
 * @param {Function} func - The callback function
 * @param {number} [wait=0] - Time delay
 * @param {Object} [options={}] - Options
 * @return {string}
 */
const debounce = function (func, wait = 0, options = {}) {
  return lodashDebounce(func, wait, options)
}

module.exports = debounce
