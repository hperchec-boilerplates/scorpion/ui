// Lodash.sample
const samplesize = require('lodash.samplesize')

/**
 * alphaNumeric
 * @description Generate random alphanumeric string based on lodash.sample function.
 * @example
 * alphaNumeric(5) // Return -> 'Gy85f' (example)
 * alphaNumeric(5, 'ABCDE') // Return -> 'AADBE' (example)
 * @param {Number} length - The length of returned random string
 * @param {Array|string} [charset] - An array or string of characters
 * @return {string}
 */
const alphaNumeric = function (length, charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
  return samplesize(charset, length).join('')
}

module.exports = {
  alphaNumeric
}
