import mobileDeviceDetect from 'mobile-device-detect'

/**
 * @vuepress
 * ---
 * title: device
 * headline: "Module: device"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Module: device
 * @module device
 * @description
 * Module: device
 * ```javascript
 * // Import
 * import device from '#modules/device'
 * ```
 */

// Object module
// Convert each mobile-device-detect property to function to proxify
const device = Object.keys(mobileDeviceDetect).reduce((object, property) => {
  object[property] = function () {
    return mobileDeviceDetect[property]
  }
  return object
}, {})

/**
 * device
 * @alias module:device
 * @typicalname device
 * @type {Object}
 * @constant
 * @description device module
 * @vuepress_syntax_block device
 * @vuepress_syntax_desc Access to device module
 * @vuepress_syntax_ctx {root}
 * this.device
 * @vuepress_syntax_ctx {component}
 * this.$root.device
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.device
 */
export default device
