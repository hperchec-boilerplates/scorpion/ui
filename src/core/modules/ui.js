// Config
import CONFIG from '@/config/config.js'

/**
 * @vuepress
 * ---
 * title: ui
 * headline: "Module: ui"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Module: ui
 * @module ui
 * @description
 * Module: ui
 * ```javascript
 * // Import
 * import ui from '#modules/ui'
 * ```
 */

/**
 * getBreakpointForWidth
 * @alias module:ui.getBreakpointForWidth
 * @description Compute breakpoint for given width
 * @param {number} width - Width
 * @return {?string} '2xl', 'xl', 'lg', 'md', 'sm', or null
 * @vuepress_syntax_block ui.getBreakpointForWidth
 * @vuepress_syntax_desc Access to ui.getBreakpointForWidth method
 * @vuepress_syntax_ctx {root}
 * this.ui.getBreakpointForWidth(...)
 * @vuepress_syntax_ctx {component}
 * this.$root.ui.getBreakpointForWidth(...)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.ui.getBreakpointForWidth(...)
 */
function getBreakpointForWidth (width) {
  return width >= __THEME_BREAKPOINTS_2XL__
    ? { name: '2xl', width: __THEME_BREAKPOINTS_2XL__ }
    : width >= __THEME_BREAKPOINTS_XL__
      ? { name: 'xl', width: __THEME_BREAKPOINTS_XL__ }
      : width >= __THEME_BREAKPOINTS_LG__
        ? { name: 'lg', width: __THEME_BREAKPOINTS_LG__ }
        : width >= __THEME_BREAKPOINTS_MD__
          ? { name: 'md', width: __THEME_BREAKPOINTS_MD__ }
          : width >= __THEME_BREAKPOINTS_SM__
            ? { name: 'sm', width: __THEME_BREAKPOINTS_SM__ }
            : { name: null, width: 0 }
}

/**
 * currentBreakpoint
 * @alias module:ui.currentBreakpoint
 * @description Return current breakpoint based on window.innerWidth
 * @return {?string} '2xl', 'xl', 'lg', 'md', 'sm', or null
 * @vuepress_syntax_block ui.currentBreakpoint
 * @vuepress_syntax_desc Access to ui.currentBreakpoint method
 * @vuepress_syntax_ctx {root}
 * this.ui.currentBreakpoint()
 * @vuepress_syntax_ctx {component}
 * this.$root.ui.currentBreakpoint()
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.ui.currentBreakpoint()
 */
function currentBreakpoint () {
  return this.CURRENT_BREAKPOINT
}

/**
 * setTheme
 * @alias module:ui.setTheme
 * @description Set theme setting and its value in Local storage
 * @param {string} theme - 'dark', 'light' or 'auto'
 * @return {void}
 * @vuepress_syntax_block ui.setTheme
 * @vuepress_syntax_desc Access to ui.setTheme method
 * @vuepress_syntax_ctx {root}
 * this.ui.setTheme(...)
 * @vuepress_syntax_ctx {component}
 * this.$root.ui.setTheme(...)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.ui.setTheme(...)
 */
function setTheme (theme) {
  // Check value
  if (theme === 'dark' || theme === 'light' || theme === 'auto') {
    localStorage.theme = theme
    this.log('system', `Switching to theme '${theme}'.`)
  } else {
    this.log('warning', `Unknown theme '${theme}'. Setting to default theme '${CONFIG.SYSTEM.DEFAULT_THEME}'...`)
    localStorage.theme = CONFIG.SYSTEM.DEFAULT_THEME
  }
  this.CURRENT_THEME = localStorage.theme
  // Set theme
  if (this.CURRENT_THEME === 'dark' || (this.CURRENT_THEME === 'auto' && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
    document.documentElement.classList.add('dark')
  } else {
    document.documentElement.classList.remove('dark')
  }
  // Reset anti-FOUC css rules on <body> element applied in the <head> section
  // of public/index.html entry file after setting theme
  document.documentElement.style.display = 'initial' // reset to initial (browser default) value
}

/**
 * currentTheme
 * @alias module:ui.currentTheme
 * @description get current theme defined in Local storage
 * @return {string} 'dark', 'light' or 'auto'
 * @vuepress_syntax_block ui.currentTheme
 * @vuepress_syntax_desc Access to ui.currentTheme method
 * @vuepress_syntax_ctx {root}
 * this.ui.currentTheme()
 * @vuepress_syntax_ctx {component}
 * this.$root.ui.currentTheme()
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.ui.currentTheme()
 */
function currentTheme () {
  return this.CURRENT_THEME
}

// Object module
const ui = {
  getBreakpointForWidth,
  currentBreakpoint,
  setTheme,
  currentTheme
}

/**
 * ui
 * @alias module:ui
 * @typicalname ui
 * @type {Object}
 * @constant
 * @description ui module
 * @vuepress_syntax_block ui
 * @vuepress_syntax_desc Access to ui module
 * @vuepress_syntax_ctx {root}
 * this.ui
 * @vuepress_syntax_ctx {component}
 * this.$root.ui
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.ui
 */
export default ui
