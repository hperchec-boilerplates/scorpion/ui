// Store
import Store from '@/store'
// Logger
import Logger from '#services/logger'

/**
 * boot
 * @description Must be called before root Vue instance creation
 * @return {void}
 */
export default async function boot () {
  Logger.consoleLog('system', 'Boot app...')
  // Get user data if authenticated
  await Store.dispatch('Auth/checkAuthenticated')
}
