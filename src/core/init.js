// Logger
import Logger from '#services/logger'

// Modules
import auth from '#modules/auth'
import device from '#modules/device'
import emit from '#modules/emit'
import log from '#modules/log'
import on from '#modules/on'
import once from '#modules/once'
import off from '#modules/off'
import ui from '#modules/ui'

const modules = {
  auth,
  device,
  emit,
  log,
  on,
  once,
  off,
  ui
}

/**
 * init
 * @ignore
 * @description App 'init' function
 * Initialize all necessary...
 * @return {void}
 */
export default async function init () {
  Logger.consoleLog('system', 'Initialize root Vue instance...')
  // Here, 'this' is Root Vue instance
  // Assign modules
  // 'proxify' function: to transform module object to 'proxied' object
  // for which all 'function' type properties are 'encapsulated' by arrow function
  // that call the function itself with the 'this' context of the root instance
  // So, if a module is an object (and not simply a function), all its child property
  // that is a function will access to 'this' root context
  const proxify = (target) => {
    const tmp = new Proxy({}, {
      // Overwrite Object type getter to 'observe'
      get: (obj, prop) => {
        if (prop in obj) {
          // If the module object property is a function, encapsulate with root 'this' context
          if (typeof obj[prop] === 'function') {
            return (...args) => { return obj[prop].call(this, ...args) }
          } else {
            return obj[prop]
          }
        }
        // Else return undefined
        return undefined
      },
      // Overwrite Object type setter
      set: function (obj, prop, value) {
        if (typeof value === 'object') {
          obj[prop] = proxify(value)
        } else {
          obj[prop] = value
        }
        return true
      }
    })
    // Loop on received object properties to start recursivity
    for (const property in target) {
      tmp[property] = target[property]
    }
    return tmp
  }
  Logger.consoleLog('system', `Define root Vue instance modules [${Object.getOwnPropertyNames(modules).join(', ')}] ...`)
  // Loop on each module and 'proxify' if it's a object
  for (const moduleName in modules) {
    let module
    if (typeof modules[moduleName] === 'object') {
      module = proxify(modules[moduleName])
    } else {
      // Else, assign simply to MyStation root instance
      module = modules[moduleName]
    }
    Object.defineProperty(this, moduleName, { get () { return module } })
  }
  // OK
  return true
}
