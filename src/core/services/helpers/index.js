/**
 * @vuepress
 * ---
 * title: Helpers
 * headline: "Service: Helpers"
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Service: Helpers
 * @module helpers
 * @description
 * Service: Helpers
 * ```javascript
 * // Import
 * import Helpers from '#services/helpers'
 * ```
 */

// Classes
import Helpers from './Helpers'

/**
 * Helpers
 * @alias module:helpers
 * @type {Helpers}
 * @constant
 * @description Helpers service
 * See also [Helpers class]{@link Helpers}
 * @vuepress_syntax_block Helpers
 * @vuepress_syntax_desc Access to Helpers instance
 * @vuepress_syntax_ctx {root}
 * this.$Helpers
 * this.$h // alias
 * @vuepress_syntax_ctx {component}
 * this.$Helpers
 * this.$h // alias
 * this.$root.$Helpers
 * this.$root.$h // alias
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.$Helpers
 * window.__ROOT_INSTANCE__.$h // alias
 */
export default Helpers
