// Logger service
import Logger from '#services/logger'
// Store
import Store from '@/store'
/**
 * vue-router 'Auth' middleware
 */
export default async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    Logger.consoleLog('router', `[AuthMiddleware] Route "${to.fullPath}" requires authentication`)
    // this route requires auth, check if logged in
    if (!Store.state.Auth.currentUser) {
      Logger.consoleLog('router', '[AuthMiddleware] Not authenticated ❌ Redirect to login page')
      // If target '/' -> no redirect
      if (to.fullPath === '/') {
        next({ path: '/login' })
      // Else, get to.fullPath to redirect after login
      } else {
        next({
          path: '/login',
          query: {
            redirect: to.fullPath
          }
        })
      }
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
}
