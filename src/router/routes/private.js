// Internationalization
import I18n from '@/i18n'

// Helpers
import Helpers from '#services/helpers'

// Settings
import Settings from '@/components/private/views/settings/Index'
import SettingsEditAccount from '@/components/private/views/settings/edit-account/Index'
import SettingsVerifyEmail from '@/components/private/views/settings/verify-email/Index'

/**
 * vue-router public routes
 */
export default [

  /**
   * Settings
   */
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    meta: {
      requiresAuth: true,
      breadcrumb: {
        schema: ['[Home]', '<this>'],
        title: (vm) => {
          return Helpers.capitalize(I18n.t('views.Settings.Index.title'))
        }
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('views.Settings.Index.title'))
      }
    }
  },
  /**
   * Settings -> edit account
   */
  {
    path: '/settings/edit-account',
    name: 'SettingsEditAccount',
    component: SettingsEditAccount,
    meta: {
      requiresAuth: true,
      breadcrumb: {
        schema: ['[Home]', '[Settings]', '<this>'],
        title: (vm) => {
          return Helpers.capitalize(I18n.t('views.Settings.EditAccount.title'))
        }
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('views.Settings.EditAccount.title'))
      }
    }
  },
  /**
   * Settings -> Verify email
   */
  {
    path: '/settings/email/verify/:hash',
    name: 'SettingsVerifyEmail',
    component: SettingsVerifyEmail,
    meta: {
      requiresAuth: true,
      breadcrumb: {
        schema: ['[Home]', '[Settings]', '<this>'],
        title: (vm) => {
          return Helpers.capitalize(I18n.t('views.VerifyEmail.title'))
        }
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('views.VerifyEmail.title'))
      }
    }
  }
]
