/**
 * Store module for users
 */

// Models
// import User from '#models/User'
// Logger
import Logger from '#services/logger'
// APIManager
import APIManager from '#services/api-manager/index'

// Define server API
const ServerAPI = APIManager.use('ServerAPI')

/**
 * Users
 * @alias module:store/modules/Users
 * @type {Object}
 */
const Users = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Users
   */
  state: {
    /**
     * createUserLoading
     * @description Indicates if request for create user is loading from API
     * @type {boolean}
     * @default false
     */
    createUserLoading: false,
    /**
     * updateUserLoading
     * @description Indicates if request for update user is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserLoading: false,
    /**
     * updateUserThumbnailLoading
     * @description Indicates if request for update user thumbnail is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserThumbnailLoading: false,
    /**
     * updateUserPasswordLoading
     * @description Indicates if request for update user password is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserPasswordLoading: false,
    /**
     * forgotPasswordLoading
     * @description Indicates if request for forgot password is loading from API
     * @type {boolean}
     * @default false
     */
    forgotPasswordLoading: false,
    /**
     * resetPasswordLoading
     * @description Indicates if request for reset password is loading from API
     * @type {boolean}
     * @default false
     */
    resetPasswordLoading: false,
    /**
     * verifyEmailLoading
     * @description Indicates if request for verify user email is loading from API
     * @type {boolean}
     * @default false
     */
    verifyEmailLoading: false,
    /**
     * resendEmailVerificationLinkLoading
     * @description Indicates if request for resend user email verification link is loading from API
     * @type {boolean}
     * @default false
     */
    resendEmailVerificationLinkLoading: false
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Users/<name>']
   */
  getters: {
    /**
     * getUser
     * @description Retrieve a user by id
     * @type {function}
     * @param {number} id - User id
     * @return {(User|undefined)}
     */
    getUser: (state) => (id) => {
      // Find user in state.users
      return state.users.find(user => user.id === id)
    }
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Users/<name>')
   */
  actions: {
    /**
     * createUser
     * @description Create user method.
     * Request `[POST] /api/users/create`
     * to create a new user.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {Object} payload.data - Request data
     * @param {string} payload.data.firstname - User firstname
     * @param {string} payload.data.lastname - User lastname
     * @param {string} payload.data.email - User email
     * @param {string} payload.data.password - User password
     * @param {string} payload.data.password_confirmation - User password
     * @return {(boolean|Error)} Error or return true
     */
    createUser: async function ({ commit, dispatch }, { data }) {
      // Set createUserLoading to true
      commit('SET_CREATE_USER_LOADING', true)
      try {
        Logger.consoleLog('system', `Create user request - Request [POST] "${ServerAPI.options.baseURL}/users/create" ...`)
        await ServerAPI.request('POST', '/users/create', { data })
      } catch (error) {
        // Set createUserLoading to false
        commit('SET_CREATE_USER_LOADING', false)
        Logger.consoleLog('error', `Create user request error - Server response [POST] "${ServerAPI.options.baseURL}/users/create" ...`)
        // Return error
        return error
      }
      Logger.consoleLog('system', 'User created ✔')
      // Set createUserLoading to false
      commit('SET_CREATE_USER_LOADING', false)
      return true
    },
    /**
     * updateUser
     * @description Update user method.
     * Request `[PATCH] /api/users/<userId>`
     * to update an user.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.userId - User id
     * @param {Object} payload.data - Request data -> user data (except: [thumbnail, password], see updateThumbnail & updateUserPassword)
     * @param {string} payload.data.firstname - User firstname
     * @param {string} payload.data.lastname - User lastname
     * @param {string} payload.data.email - User email
     * @param {string} payload.data.password - User password (for confirmation)
     * @return {(boolean|Error)} Error or return true
     */
    updateUser: async function ({ commit }, { userId, data }) {
      // Set updateUserLoading to true
      commit('SET_UPDATE_USER_LOADING', true)
      try {
        Logger.consoleLog('system', `Update user request - Request [PATCH] "${ServerAPI.options.baseURL}/users/${userId}" ...`)
        await ServerAPI.request('PATCH', `/users/${userId}`, { data })
      } catch (error) {
        // Set updateUserLoading to false
        commit('SET_UPDATE_USER_LOADING', false)
        Logger.consoleLog('error', `Update user request error - Server response [PATCH] "${ServerAPI.options.baseURL}/users/${userId}" ...`)
        return error
      }
      // Set updateUserLoading to false
      commit('SET_UPDATE_USER_LOADING', false)
      return true
    },
    /**
     * updateUserThumbnail
     * @description Update user thumbnail method.
     * Request `[POST] /api/users/<userId>/thumbnail`
     * to update the thumbnail of an user.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.userId - User id
     * @param {Object} payload.data - Method payload -> formData
     * @return {(boolean|Error)} Error or return true
     */
    updateUserThumbnail: async function ({ commit }, { userId, data }) {
      // Set updateUserThumbnailLoading to true
      commit('SET_UPDATE_USER_THUMBNAIL_LOADING', true)
      try {
        Logger.consoleLog('system', `Update user thumbnail request - Request [POST] "${ServerAPI.options.baseURL}/users/${userId}/thumbnail" ...`)
        await ServerAPI.request('POST', `/users/${userId}/thumbnail`, {
          data,
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
      } catch (error) {
        // Set updateUserThumbnailLoading to false
        commit('SET_UPDATE_USER_THUMBNAIL_LOADING', false)
        Logger.consoleLog('error', `Update user thumbnail request error - Server response [POST] "${ServerAPI.options.baseURL}/users/${userId}/thumbnail" ...`)
        return error
      }
      // Set updateUserThumbnailLoading to false
      commit('SET_UPDATE_USER_THUMBNAIL_LOADING', false)
      return true
    },
    /**
     * updateUserPassword
     * @description Update user password method.
     * Request `[PATCH] /api/me/password`
     * to update the password of an user.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {Object} payload.data - Data
     * @param {string} payload.data.current_password - Current password
     * @param {string} payload.data.password - New password
     * @param {string} payload.data.password_confirmation - New password confirmation
     * @return {(boolean|Error)} Error or return true
     */
    updateUserPassword: async function ({ commit }, { data }) {
      // Set updateUserPasswordLoading to true
      commit('SET_UPDATE_USER_PASSWORD_LOADING', true)
      try {
        Logger.consoleLog('system', `Update user password request - Request [PATCH] "${ServerAPI.options.baseURL}/me/password" ...`)
        await ServerAPI.request('PATCH', '/me/password', { data })
      } catch (error) {
        // Set updateUserPasswordLoading to false
        commit('SET_UPDATE_USER_PASSWORD_LOADING', false)
        Logger.consoleLog('error', `Update user password request error - Server response [PATCH] "${ServerAPI.options.baseURL}/me/password" ...`)
        return error
      }
      // Set updateUserPasswordLoading to false
      commit('SET_UPDATE_USER_PASSWORD_LOADING', false)
      return true
    },
    /**
     * forgotPassword
     * @description Send reset password request with email.
     * Request `[POST] /api/forgot-password`
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.email - User email
     * @return {(boolean|Error)} Error or return true
     */
    forgotPassword: async function ({ commit, dispatch }, payload) {
      // Set forgotPasswordLoading to true
      commit('SET_FORGOT_PASSWORD_LOADING', true)
      try {
        await ServerAPI.request('POST', '/forgot-password', {
          data: payload
        })
      } catch (error) {
        // Set forgotPasswordLoading to false
        commit('SET_FORGOT_PASSWORD_LOADING', false)
        return error
      }
      // Set forgotPasswordLoading to false
      commit('SET_FORGOT_PASSWORD_LOADING', false)
      return true
    },
    /**
     * resetPassword
     * @description Reset user password method.
     * Request `[POST] /api/reset-password`
     * to reset a user password.
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.email - User email
     * @param {string} payload.password - New password
     * @param {string} payload.password_confirmation - New password confirmation
     * @return {(boolean|Error)} Error or return true
     */
    resetPassword: async function ({ commit, dispatch }, payload) {
      // Set resetPasswordLoading to true
      commit('SET_RESET_PASSWORD_LOADING', true)
      try {
        await ServerAPI.request('POST', '/reset-password', {
          data: payload
        })
      } catch (error) {
        // Set resetPasswordLoading to false
        commit('SET_RESET_PASSWORD_LOADING', false)
        return error
      }
      // Set resetPasswordLoading to false
      commit('SET_RESET_PASSWORD_LOADING', false)
      return true
    },
    /**
     * verifyEmail
     * @description Verify user email method.
     * Request `[POST] /api/users/<userId>/email/verify/<hash>?expires=<expires>&signature=<signature>`
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {Object} payload.userId - User id
     * @param {Object} payload.hash - Hash
     * @param {Object} payload.query.expires - When route expires
     * @param {string} payload.query.signature - The route signature
     * @return {(boolean|Error)} Error or return true
     */
    verifyEmail: async function ({ commit }, payload) {
      // Set verifyEmailLoading to true
      commit('SET_VERIFY_EMAIL_LOADING', true)
      try {
        Logger.consoleLog('system', `Verify email request - Request [POST] "${ServerAPI.options.baseURL}/users/${payload.userId}/email/verify/${payload.hash}?expires=${payload.query.expires}&signature=${payload.query.signature}" ...`)
        await ServerAPI.request('POST', `/users/${payload.userId}/email/verify/${payload.hash}?expires=${payload.query.expires}&signature=${payload.query.signature}`)
      } catch (error) {
        // Set verifyEmailLoading to false
        commit('SET_VERIFY_EMAIL_LOADING', false)
        Logger.consoleLog('error', `Verify email request error - Server response [POST] "${ServerAPI.options.baseURL}/users/${payload.userId}/email/verify/${payload.hash}?expires=${payload.query.expires}&signature=${payload.query.signature}" ...`)
        return error
      }
      // Set verifyEmailLoading to false
      commit('SET_VERIFY_EMAIL_LOADING', false)
      return true
    },
    /**
     * resendEmailVerificationLink
     * @description Resend user email verification link method.
     * Request `[POST] /api/users/<userId>/email/reset-verification-link`
     * @param {Object} context - vuex context
     * @param {Object} payload - Method payload
     * @param {string} payload.userId - User id
     * @return {(boolean|Error)} Error or return true
     */
    resendEmailVerificationLink: async function ({ commit }, { userId }) {
      // Set resendEmailVerificationLinkLoading to true
      commit('SET_RESEND_EMAIL_VERIFICATION_LINK_LOADING', true)
      try {
        Logger.consoleLog('system', `Resend email verification link request - Request [POST] "${ServerAPI.options.baseURL}/users/${userId}/email/resend-verification-link" ...`)
        await ServerAPI.request('POST', `/users/${userId}/email/resend-verification-link`)
      } catch (error) {
        // Set resendEmailVerificationLinkLoading to false
        commit('SET_RESEND_EMAIL_VERIFICATION_LINK_LOADING', false)
        Logger.consoleLog('error', `Resend email verification link request error - Server response [POST] "${ServerAPI.options.baseURL}/users/${userId}/email/resend-verification-link" ...`)
        return error
      }
      // Set resendEmailVerificationLinkLoading to false
      commit('SET_RESEND_EMAIL_VERIFICATION_LINK_LOADING', false)
      return true
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Users/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_CREATE_USER_LOADING
     * @description Mutate state.createUserLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CREATE_USER_LOADING (state, value) {
      state.createUserLoading = value
    },
    /**
     * SET_UPDATE_USER_LOADING
     * @description Mutate state.updateUserLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_LOADING (state, value) {
      state.updateUserLoading = value
    },
    /**
     * SET_UPDATE_USER_THUMBNAIL_LOADING
     * @description Mutate state.updateUserThumbnailLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_THUMBNAIL_LOADING (state, value) {
      state.updateUserThumbnailLoading = value
    },
    /**
     * SET_UPDATE_USER_PASSWORD_LOADING
     * @description Mutate state.updateUserPasswordLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_PASSWORD_LOADING (state, value) {
      state.updateUserPasswordLoading = value
    },
    /**
     * SET_FORGOT_PASSWORD_LOADING
     * @description Mutate state.forgotPasswordLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_FORGOT_PASSWORD_LOADING (state, value) {
      state.forgotPasswordLoading = value
    },
    /**
     * SET_RESET_PASSWORD_LOADING
     * @description Mutate state.resetPasswordLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_RESET_PASSWORD_LOADING (state, value) {
      state.resetPasswordLoading = value
    },
    /**
     * SET_VERIFY_EMAIL_LOADING
     * @description Mutate state.verifyEmailLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_VERIFY_EMAIL_LOADING (state, value) {
      state.verifyEmailLoading = value
    },
    /**
     * SET_RESEND_EMAIL_VERIFICATION_LINK_LOADING
     * @description Mutate state.resendEmailVerificationLinkLoading
     * @param {Object} state - vuex store state
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_RESEND_EMAIL_VERIFICATION_LINK_LOADING (state, value) {
      state.resendEmailVerificationLinkLoading = value
    }
  }
}

export default Users
