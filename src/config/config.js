// Color
const Color = require('color')
// snakecase
const snakecase = require('lodash.snakecase')

module.exports = {
  /**
   * System
   */
  SYSTEM: {
    /**
     * Define if logger verbose in production mode (must be true or false)
     * @default false
     * @type {boolean}
     */
    LOG: true,
    /**
     * Types configuration for Logger
     * @type {Object}
     */
    LOG_TYPES: {
      info: {
        messageBadge: '[INFO]',
        messageBadgeColor: Color('blue').lighten(0.4).hex(),
        messageColor: __THEME_PRIMARY_COLOR__
      },
      system: {
        messageBadge: '[SYSTEM]',
        messageBadgeColor: '#D6B38B',
        messageColor: '#AB8F6F'
      },
      router: {
        messageBadge: '[ROUTER]',
        messageBadgeColor: Color('purple').lighten(0.8).hex(),
        messageColor: 'purple'
      },
      advice: {
        messageBadge: '[ADVICE]',
        messageBadgeColor: '#122299',
        messageColor: __THEME_PRIMARY_COLOR__
      },
      warning: {
        messageBadge: '[WARN]',
        messageBadgeColor: __THEME_WARNING_COLOR__,
        messageColor: Color(__THEME_WARNING_COLOR__).darken(0.5).hex()
      },
      error: {
        messageBadge: '[ERROR]',
        messageBadgeColor: __THEME_ERROR_COLOR__,
        messageColor: __THEME_ERROR_COLOR__
      },
      default: {
        messageBadge: '[LOG]',
        messageBadgeColor: '#FFFF00',
        messageColor: __THEME_PRIMARY_COLOR__
      }
    },
    /**
     * Default theme
     * @type {string}
     */
    DEFAULT_THEME: 'light' // 'light', 'dark' or 'auto'
  },
  /**
   * Internationalization
   */
  I18N: {
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    LOCALE: 'fr',
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    FALLBACK_LOCALE: 'fr'
  },
  /**
   * APIs
   */
  API: {
    /**
     * Server API as Axios Instance options
     * @type {Object}
     */
    ServerAPI: {
      baseURL: '/api',
      allowedMethods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      errorResponseKey: snakecase(__APP_NAME__ + '_error_response')
    }
  },
  /**
   * Authentication
   */
  AUTH: {
    /**
     * oAuth access token identifier (in browser LocalStorage)
     * @default 'app_access_token'
     * @type {string}
     */
    ACCESS_TOKEN_NAME: 'app_access_token',
    /**
     * oAuth refresh token identifier (in browser LocalStorage)
     * @default 'app_refresh_token'
     * @type {string}
     */
    REFRESH_TOKEN_NAME: 'app_refresh_token'
  },
  /**
   * Development options
   */
  DEVELOPMENT: {
    /**
     * System options in development mode
     */
    SYSTEM: {
      /**
       * Define if logger verbose in development mode (must be true or false)
       * @default true
       * @type {boolean}
       */
      LOG: true
    },
    /**
     * Define server API in development mode
     */
    API: {
      ServerAPI: {
        URL: 'http://localhost:8000', // Local server url
        APIBaseURL: 'http://localhost:8000/api'
      }
    }
  }
}
