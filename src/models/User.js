// APIManager
import APIManager from '#services/api-manager/index'
// utils
import { isDef } from '#utils'

// Define server API
const ServerAPI = APIManager.use('ServerAPI')

const DEFAULTS = (prop) => User.defaults()[prop]

/**
 * User class
 */
class User {
  /**
   * @private
   */
  _id;
  _firstname;
  _lastname;
  _email;
  _thumbnailFilename;
  _thumbnailURL;
  _thumbnailBase64;
  _role;
  _createdAt;
  _updatedAt;
  _emailVerifiedAt;
  _lastLogin;

  /**
   * Create a User
   * @param {Object} data - Object that represents the user
   * @param {number} data.id - The unique user id
   * @param {string} data.firstname - The user firstname
   * @param {string} data.lastname - The user lastname
   * @param {string} data.email - The user email
   * @param {string} data.thumbnail - The user thumbnail (filename)
   * @param {string} data.role - The user role
   * @param {?string} data.created_at - When the user has been created
   * @param {?string} data.updated_at - When the user has been updated
   * @param {?string} data.email_verified_at - When the user email has been verified
   * @param {?string} data.last_login - User last login date or null
   */
  constructor (data) {
    // If data is undefined -> called as `new User()`
    const __EMPTY__ = !isDef(data)
    data = __EMPTY__ ? DEFAULTS : data
    // Assign properties
    this._id = data.id
    this._firstname = data.firstname
    this._lastname = data.lastname
    this._email = data.email
    this._thumbnailFilename = data.thumbnail
    this._thumbnailURL = this.id ? `${ServerAPI.options.baseURL}/users/${this.id}/thumbnail` : null
    this._thumbnailBase64 = null // Init to null
    if (this.id) { this.setThumbnailBase64() } // Load async base64 thumbnail data
    this._role = data.role
    this._createdAt = new Date(data.created_at)
    this._updatedAt = new Date(data.updated_at)
    this._emailVerifiedAt = new Date(data.email_verified_at)
    this._lastLogin = data.last_login
  }

  /**
   * Accessors & mutators
   */

  /**
   * id
   * @category properties
   * @readonly
   * @description User id accessor
   * @type {number}
   */
  get id () {
    return this._id
  }

  /**
   * firstname
   * @category properties
   * @readonly
   * @description User firstname accessor
   * @type {string}
   */
  get firstname () {
    return this._firstname
  }

  /**
   * lastname
   * @category properties
   * @readonly
   * @description User lastname accessor
   * @type {string}
   */
  get lastname () {
    return this._lastname
  }

  /**
   * email
   * @category properties
   * @readonly
   * @description User email accessor
   * @type {string}
   */
  get email () {
    return this._email
  }

  /**
   * birthDate
   * @category properties
   * @readonly
   * @description User birthDate accessor
   * @type {Date}
   */
  get birthDate () {
    return this._birthDate
  }

  /**
   * role
   * @category properties
   * @readonly
   * @description User role accessor
   * @type {string}
   */
  get role () {
    return this._role
  }

  /**
   * thumbnailFilename
   * @category properties
   * @readonly
   * @description User thumbnail filename accessor
   * @type {string}
   */
  get thumbnailFilename () {
    return this._thumbnailFilename
  }

  /**
   * thumbnailURL
   * @category properties
   * @readonly
   * @description User thumbnail URL accessor
   * @type {string}
   */
  get thumbnailURL () {
    return this._thumbnailURL
  }

  /**
   * thumbnailBase64
   * @category properties
   * @readonly
   * @description User base64 thumbnail data accessor
   * @type {string}
   */
  get thumbnailBase64 () {
    return this._thumbnailBase64
  }

  /**
   * createdAt
   * @category properties
   * @readonly
   * @description User createdAt accessor
   * @type {Date}
   */
  get createdAt () {
    return this._createdAt
  }

  /**
   * updatedAt
   * @category properties
   * @readonly
   * @description User updatedAt accessor
   * @type {Date}
   */
  get updatedAt () {
    return this._updatedAt
  }

  /**
   * emailVerifiedAt
   * @category properties
   * @readonly
   * @description User emailVerifiedAt accessor
   * @type {Date}
   */
  get emailVerifiedAt () {
    return this._emailVerifiedAt
  }

  /**
   * lastLogin
   * @category properties
   * @readonly
   * @description User lastLogin accessor
   * @type {Object[]}
   */
  get lastLogin () {
    return this._lastLogin
  }

  /**
   * Methods
   */

  /**
   * setThumbnailBase64
   * @category methods
   * @description Set base64 thumbnail data (async request: [GET] <ServerAPIBaseURL>/users/<id>/thumbnail)
   * @return {void}
   */
  async setThumbnailBase64 () {
    const toDataURL = async function (file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.onload = () => {
          resolve(reader.result)
        }
        reader.onerror = reject
        reader.readAsDataURL(file)
      })
    }
    const response = await ServerAPI.request('GET', this.thumbnailURL, { responseType: 'blob' })
    this._thumbnailBase64 = await toDataURL(response.data)
  }

  /**
   * fullName
   * @category methods
   * @description Returns concatenated firstname and lastname with a white space
   * @return {string}
   */
  fullName () {
    return this.firstname + ' ' + this.lastname
  }

  /**
   * isAdmin
   * @category methods
   * @description Return true if user is admin
   * @return {boolean}
   */
  isAdmin () {
    return this.role === 'admin'
  }

  /**
   * Defaults
   */

  /**
   * Default properties for a new empty 'User' object
   * @return {Object} Default properties object
   */
  static defaults () {
    return {
      id: null,
      firstname: null,
      lastname: null,
      email: null,
      thumbnail: null,
      role: 'user',
      created_at: null,
      updated_at: null,
      email_verified_at: null
    }
  }
}

export default User
